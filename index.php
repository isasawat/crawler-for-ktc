<?php
$url = 'http://10.26.11.193:8983/solr/promotion/select?q=*%3A*&wt=json&facet=true&facet.field=category_t&rows=0';
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HEADER, false);
$data = curl_exec($curl);
curl_close($curl);
$data_json = json_decode($data);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>SEIM</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
   <div class="row">
       <div class="col-xs-12">
            <ul class="nav nav-pills">
            <?php 
            $category_t = $data_json->facet_counts->facet_fields->category_t;
            $cat_label = current($category_t);
            while($cat_label): ?>
                <li role="presentation"><a href="javascript:void(0);"><?php echo $cat_label; ?><span class="label label-primary"><?php echo next($category_t); ?></span></a></li>
                <?php $cat_label = next($category_t); ?>
            <?php endwhile; ?>
            </ul>
       </div>
   </div>
    <div class="row">
        <div class="col-xs-12">
            <form id="search-text-form" class="form-inline" action="search.php">
                <div class="form-group text-center" style="width: 100%; margin-top: 150px;">
                    <label class="sr-only" for="search-text">Search Text</label>
                    <input type="text" name="text" class="form-control" id="search-text" style="width:50%;" placeholder="What would you like to find?">
                    <button type="submit" class="btn btn-primary" data-loading-text="Searching..."><span class="glyphicon glyphicon-search"></span></button>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div id="search-result-container" class="col-xs-10 col-xs-offset-1">
            
        </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery-1.11.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/all.js"></script>
  </body>
</html>