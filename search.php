<?php
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

$q = urlencode('{!boost b=recip(ms(NOW,expire_dt),3.16e-11,1,1)}*'.$_REQUEST['text']);
$qf = urlencode('title_t^2 desc_s');
//$q = '*:*';
//$url = 'http://10.26.11.193:8983/solr/gettingstarted_shard1_replica2/select?q='.$q.'&start=0&rows=10&wt=json';
$url = 'http://10.26.11.193:8983/solr/promotion/select?q='.$q.'&wt=json&q=test&qf='.$qf.'&fl=*,score';
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HEADER, false);
$data = curl_exec($curl);
curl_close($curl);
$data_json = json_decode($data);
foreach($data_json->response->docs as $doc): ?>
<div class="media">
  <div class="media-body">
    <h4 class="media-heading"><?php echo current($doc->title_t); ?> (<?php echo $doc->score; ?>)</h4>
    <h6><?php echo current($doc->category_t); ?></h6>
    <p><?php echo $doc->desc_s; ?></p>
      <a href="<?php echo $doc->url_s; ?>">More Detail</a>
  </div>
</div>
<?php
endforeach;