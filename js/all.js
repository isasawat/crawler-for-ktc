$(function () {
    $('#search-text-form').submit(function (event) {
        event.preventDefault();
        
        var $form = $(this);
        var url = $form.attr('action');
        var $submitBtn = $form.find('[type="submit"]');
        $submitBtn.button('loading');
        
        $.post(url,$form.serialize(),function(data){
            $submitBtn.button('reset');
            $('#search-result-container').empty().append(data);
        });
    });
});