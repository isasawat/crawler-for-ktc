<?php

/* Most of the code from https://subinsb.com/how-to-create-a-simple-web-crawler-in-php
 * Thanks, Subin
 *
 * This crawler's taylored only for KTC promotion site only
 */

include("simple_html_dom.php");
$crawled_urls=array();
$found_urls=array();

ini_set("memory_limit","512M");

function rel2abs($rel, $base){
	if (parse_url($rel, PHP_URL_SCHEME) != ''){
		return $rel;
	}
	if ($rel[0]=='#' || $rel[0]=='?'){
		return $base.$rel;
	}
	extract(parse_url($base));
	$path = preg_replace('#/[^/]*$#', '', $path);
	if ($rel[0] == '/'){
		$path = '';
	}
	$abs = "$host$path/$rel";
	$re = array('#(/.?/)#', '#/(?!..)[^/]+/../#');
	for($n=1; $n>0;$abs=preg_replace($re,'/', $abs,-1,$n)){}
	$abs=str_replace("../","",$abs);
	return $scheme.'://'.$abs;
}

function perfect_url($u,$b){
	$bp=parse_url($b);
    
	if(($bp['path']!="/" && $bp['path']!="") || $bp['path']==''){
		if($bp['scheme']==""){
			$scheme="http";
		}else{
			$scheme=$bp['scheme'];
		}
//		$b=$scheme."://".$bp['host']."/";
        $b=$scheme."://".$bp['host'].$bp['path'];
	}
    
	if(substr($u,0,2)=="//"){
		$u="http:".$u;
	}
	if(substr($u,0,4)!="http"){
		$u=rel2abs($u,$b);
	}
	return $u;
}

function crawl_site($u){
	global $crawled_urls, $found_urls;
    set_time_limit(600);
	$uen=urlencode($u);
	// if((array_key_exists($uen,$crawled_urls)==0 || $crawled_urls[$uen] < date("YmdHis",strtotime('-25 seconds', time())))){
	if(array_key_exists($uen,$crawled_urls)==0){
		$html = file_get_html($u);
		$crawled_urls[$uen]=date("YmdHis");
		if (strpos($u,'Promotion-detail') === false) { //not at promo detail page, find target link to travel to
			foreach($html->find("a") as $li){
				$url=perfect_url($li->href,$u);
				$enurl=urlencode($url);

				if($url!='' && substr($url,0,4)!="mail" && substr($url,0,4)!="java" && array_key_exists($enurl,$found_urls)==0){
					if (strpos($url,'NextRow') === false && strpos($url,'Promotion-detail') === false) {
						continue; //not pagination link and detail page link, just skip it
					}
                    
                    echo('following: '.$url);
                    $found_urls[$enurl]=1;
                    unset($html);
					crawl_site($url);
				}
			}
		}
		else { // at promo detail page, gather info
//			var_dump($u);
			$th_months = array("ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
			$col = $html->find('.two-col1', 0);
			$title = $col->find('.headline-h2 h2',0)->plaintext;
			$data_block = $col->find('.corner-lightgray');
			$timestamp = strtotime('2015-12-31');
			$detail = '';
			foreach($data_block as $block) {
				$data_text = trim($block->plaintext);
				
				if (strpos($data_text,'Period :') !== false) {
					$th_date = mb_substr($data_text,-10,null,'utf-8');
					$th_date_chunk = explode(' ',$th_date);
					$month = array_search($th_date_chunk[1],$th_months);
					if ($month !== false) {
						$str_time = (intval('25'.$th_date_chunk[2])-543) .'-'. ($month+1) .'-'. $th_date_chunk[0];
						$timestamp = strtotime($str_time);
					}
				}
				else if (mb_strpos($data_text,'Promotion Details',0,'utf-8') !== false) {
                    $detail_formatted = mb_substr($data_text,19,-23,'utf-8');
					$detail = trim($detail_formatted);
				}
				
			}
			
			$params = array(array(
				'id'=>uniqid('',true),
				'title_t'=>$title,
				'desc_s'=>$detail,
				'expire_dt'=>date(DATE_ISO8601,$timestamp),
				'category_t'=>$_REQUEST['category'],
				'url_s'=>$u,
			));
			$json_param = json_encode($params);
//            var_dump($json_param);
            
			$url = 'http://10.26.11.193:8983/solr/promotion/update/';
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($curl, CURLOPT_POSTFIELDS, $json_param);
			
			$data = curl_exec($curl);
			curl_close($curl);
		}
	}
}

if (isset($_REQUEST['url'])) {
    if (!empty($_REQUEST['url'])) {
        global $crawled_urls, $found_urls;
        crawl_site($_REQUEST['url']);
        
        var_dump($crawled_urls);
        var_dump($found_urls);
    }
}